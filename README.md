# Contract Testing a Monolith

## Description

This repository demonstrates a very basic example of a consumer-driven contract
testing workflow/integration into a CI/CD pipeline, where the consumer and
provider exist in the same repository. It utilises the free version of Pactflow,
but also includes the scripts and commands for local testing/using the Pact
broker docker image. You can read more about that [here](https://github.com/pact-foundation/pact_broker#usage).

The application itself is a simple audit log table, pulling entries from a
PostgreSQL DB into the UI. It exists for the purposes of contract testing demos
and has only endpoint with two methods (`GET` and `POST` to `/log`).
An additional endpoint has been added to manage provider states as part of the
provider-side tutorial.

## How to Use this Repository

1. **Follow along with the tutorial**
  - Fork a copy of the root application repo and head to the [/docs](/docs/)
    folder to follow the tutorials.
    The tutorials cover:
    - Writing a consumer side test in Pact-JS
    - Publishing the pact
    - Verifying the pact against the provider with Pact-Python
    - Integrating it all into a Gitlab CI/CD pipeline
2. **I just came here for the examples**
  - Then here's a list of key files to get you started

Key Files:

- Consumer Side
  - [Consumer test](front-end/tests/contract/consumer/frontend-backend.spec.js)
  - [Pact generated from Consumer test](front-end/tests/contract/pacts/frontend-backend.json)
  - [Manual publish script](front-end/tests/helpers/pact-publish.js)
- Provider Side
  - [Provider verification - CLI command outlined in Makefile for local running](Makefile)
  - [Provider state JSON](back-end/tests/contract/provider/states/)
  - [Helper DB dump script for creating states](back-end/tests/helpers/pg-dump.py)
  - [DB data generation script](back-end/scripts/make_log_entries.py)
- CI/CD Pipeline Integration
  - [Gitlab CI file](.gitlab-ci.yml)

## Getting Started

### Installing Dependencies

The front-end service is written in NodeJS, and dependency management is handled
through npm.
The back-end service is written in Python, and dependency management is handled
through python venvs and the requirements.txt file.

- Front-end service - from the front-end service folder, run `npm i`
- Back-end service - from the back-end service folder, ensure you're in a python3 venv
  then run `pip install -r requirements.txt`

### Running the Application

The whole stack can be run in docker using the following commands:
(Note: profile commands require at least Docker-Compose 1.28)

- `docker-compose up -d` to bring up all the application containers
- `docker-compose up --build back-end` to bring up the back-end API and
  dependent DB

**Note:** If you see errors in the `back-end` logs of the nature of 
`relation audit_log does not exist`, try running `docker-compose restart back-end`
to restart that service and re-run the migrations.

The front-end will be available at http://localhost:8081

The API will be available at http://localhost:5000 and the API docs at 
http://localhost:5000/docs

There is a helper script to populate the application database with some log
entries with the following command:
`docker-compose exec back-end scripts/make_log_entries.py`

The Makefile also has some commands which can be run from the root directory.

<!-- ## Contract Testing

Contract testing has been implemented using PACT-JS and PACT-Python for the front-end
and back-end services respectively.

There are makefile commands to simplify the running of the tests across the two:
- `make test-pact-consumer` runs the consumer side test, and creates a pact file
  in front-end/tests/contract/pacts
- `make test-pact-provider` runs the provider side verification. This can be
  against a pact file or a published pact in the Pact Broker by changing the
  `--pact-url`
    - To run against a local pact file, change the `--pact-url` to the relative file
      path including the filename
      (e.g. `--pact-url=../front-end/tests/contract/pacts/frontend-backend.json`)
    - To run against a published pact in the broker, the `--pact-url` needs to point
      to an endpoint with the following pattern:
      `http://localhost:9292/pacts/provider/<PROVIDER-NAME>/consumer/<CONSUMER-NAME>/version/<CONSUMER-APP-VERSION-#>`
    - **NB.** Ensure that the provider version is accurate when verifying. Change this
      by altering the `-a` flag value
- `make test-pact` runs the consumer test and provider verification steps in one 
  (provider notes are relevant here too). -->

## CI/CD Pipeline

This repo uses GitLab for its CI/CD and it has the following stages:
- lint
- test
    - (consumer side) pact-test
- publish
    - pact-publish
- verify
    - (provider side) pact-verify
- deploy
    - can-i-deploy

Further details on the developmental workflow for this pipeline can be found in
the CI/CD tutorial in the `/docs` folder.