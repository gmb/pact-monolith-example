"""Add audit log table.

Revision ID: cec20ed88750
Revises:
Create Date: 2022-02-01 14:51:52.740643

"""
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql as pg

from alembic import op

# revision identifiers, used by Alembic.
revision = "cec20ed88750"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "audit_log",
        sa.Column("id", pg.UUID, primary_key=True),
        sa.Column("timestamp", sa.DateTime, nullable=False),
        sa.Column("event_source", sa.String, nullable=False),
        sa.Column("event_type", sa.String, nullable=False),
        sa.Column("message", sa.String, nullable=False),
        sa.Column("additional_data", pg.JSONB, nullable=False),
    )


def downgrade():
    op.drop_table("audit_log")
