from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class AuditLogWrite(BaseModel):
    """A model for writing to the audit log."""

    timestamp: Optional[datetime]
    event_source: str
    event_type: str
    message: str


class AuditLogRead(BaseModel):
    """A read-only version of audit log."""

    id: UUID
    timestamp: datetime
    event_source: str
    event_type: str
    message: str

    class Config:
        orm_mode = True
