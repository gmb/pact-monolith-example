import axios from 'axios'
import base64 from 'base-64'
import env from '../../env.js'

function getAuth () {
  const authStr = `${env.API_USER}:${env.API_PASSWORD}`
  return base64.encode(authStr)
}

async function _fetchLogs (api_url) {
  console.log(env)
  console.log(`Accesing ${api_url}/log`)
  const result = await axios.get(
      `${api_url}/log`, {
        headers: {
          Authorization: `Basic ${getAuth()}`
        }
      }
  )
  console.log(result)
  return result
}

async function fetchLogs (api_url) {
  const result = await _fetchLogs(api_url)
  return result.data
}

export { _fetchLogs, fetchLogs }
