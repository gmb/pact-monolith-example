import { createApp } from 'vue'
import App from './App.vue'
import PrimeVue from 'primevue/config'
import DataTable from 'primevue/datatable'
import Column from 'primevue/column'
import LogTable from '@/components/LogTable.vue'

createApp(App)
  .use(PrimeVue)
  .component('DataTable', DataTable)
  .component('Column', Column)
  .component('LogTable', LogTable)
  .mount('#app')
