const { Publisher } = require("@pact-foundation/pact")

// For hosted pact brokers
const opts = {
  pactBroker: {$PACT_BROKER_URL},
  pactBrokerToken: {$PACT_BROKER_TOKEN},
  consumerVersion: process.env.GIT_COMMIT,
  pactFilesOrDirs: ['../contract/pacts'],
  tags: ["dev"],
};

// If using a local pact broker
// const opts = {
//   pactFilesOrDirs: [path.resolve(process.cwd(), "front-end/tests/contract/pacts")],
//   pactBroker: "http://localhost:8080",
//   consumerVersion: "1.0.1",
//   providerVersion: "1.0.1",
//   tags: "dev"
// }


new Publisher(opts).publishPacts()