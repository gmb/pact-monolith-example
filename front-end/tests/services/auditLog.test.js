import { fetchLogs } from '../../src/services/auditLog'

test('fetchLogs returns data', () => {
  return fetchLogs().then(data => {
    expect(data).toHaveLength(2)
  })
})
